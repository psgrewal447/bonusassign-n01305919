﻿<%@ Page Title="Divisible Sizzable" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DivisibleCheck.aspx.cs" Inherits="BonusAssign_N01305919.About" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="text-align:center" >Enter number to check wheather a number is prime number or not</h2>
    <div style="text-align:center" >
        <asp:Label runat="server">Enter number  </asp:Label>
        <asp:TextBox runat="server" ID="InputNumber"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputNumberRequired" runat="server" ControlToValidate="InputNumber" ErrorMessage="Please enter number" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="InputNumberRegularExpression" runat="server" ControlToValidate="InputNumber" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[0-9]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitUserInputNumber" Text="Is it prime number or not" runat="server" OnClick="Divisible_check"/>
    </div>
    <div style="text-align:center" id="prime_check" runat="server"></div>
</asp:Content>
