﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssign_N01305919
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    protected void CheckforPalindrome(object sender, EventArgs e)
        {
        if (!Page.IsValid)
         {
                return;
         }
            //conditions for palindrome is refered from https://www.c-sharpcorner.com/blogs/program-to-check-whether-a-string-palindrome-is-or-not1 and https://chandradev819.wordpress.com/2011/04/28/how-to-check-palindrome-in-asp-net/
            string enterInput = validateString.Text;
            string lowecaseinput = enterInput.ToLower();
            string whitespaceinput = lowecaseinput.Replace(" ", string.Empty);
            string validateStringInReverse = string.Empty;
            int length = whitespaceinput.Length;
            bool flag = false;
            for (int i = 0;i < length/2;i++)
           {
                if (whitespaceinput[i] != whitespaceinput[length - i - 1])
                {
                    flag = true;
                    break;
                }
            }
if (flag == false)
     {
        palidrome_check.InnerHtml = "<strong><em>Result:</em></strong> \"" + enterInput + "\" is a Palindrome String";
     }
else
     {
       palidrome_check.InnerHtml = "<strong><em>Result:</strong></em> \"" + enterInput + "\" is not a Palindrome String";
     }
        }

    }
}