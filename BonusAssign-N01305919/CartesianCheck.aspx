﻿<%@ Page Title="Cartesian Smartesian" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CartesianCheck.aspx.cs" Inherits="BonusAssign_N01305919.Cartesian" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="text-align:center" >Enter two numbers to check that they lies in which quardant</h2>
     <div style="text-align:center">
        <asp:Label runat="server">Enter x-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="valueforXaxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputXAxisRequired" runat="server" ControlToValidate="valueforXaxis" ErrorMessage="please enter number" ></asp:RequiredFieldValidator>
         <asp:CustomValidator ID="valueforXAxisCustom" runat="server" ControlToValidate="valueforXaxis" OnServerValidate="valueforXAxisCustom_ServerValidate" ErrorMessage="Value for x-axis should not be zero" ></asp:CustomValidator>
        <asp:RegularExpressionValidator ID="valueforXAxisRegular" runat="server" ControlToValidate="valueforXaxis" ErrorMessage="Please enter a valid number " ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ></asp:RegularExpressionValidator>
        
        <br />
        <asp:Label runat="server">Enter y-axis value: </asp:Label>
        <asp:TextBox runat="server" ID="valueforYaxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valueforYAxisRequired" runat="server" ControlToValidate="valueforYaxis" ErrorMessage="please enter  number" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
         <asp:CustomValidator ID="valueforYAxisCustom" runat="server" ControlToValidate="valueforYaxis" OnServerValidate="valueforYAxisCustom_ServerValidate" ErrorMessage="Value for y-axis should not be zero"  ></asp:CustomValidator>
        <asp:RegularExpressionValidator ID="valueforYAxisRegular" runat="server" ControlToValidate="valueforYaxis" ErrorMessage="Please enter a valid number" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ></asp:RegularExpressionValidator>
        
        <br />
        <br />
        <asp:Button ID="submit" Text="Value in quardant" runat="server" OnClick="Quardant"/>
    </div>
    <div style="text-align:center" id="Quadrant_check" runat="server"></div>
    
</asp:Content>
