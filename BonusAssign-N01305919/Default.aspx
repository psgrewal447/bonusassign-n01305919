﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BonusAssign_N01305919._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Bonus Assignment-N01305919</h1>
        <p class="lead">This assignment is focused on using programming concepts to solve problems. You will create
                        a different web form for each problem, and use server control inputs with validators to determine
                        that the input is indeed valid.</p>
        
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>1. Cartesian Smartesian</h2>
            <p>
                We determine a point on this quadrant by a (x, y) value, where x and y are both integers. A
                positive x value means that the point will fall to the right of the y-axis (vertical line). A positive y
                value means that the point will fall above the x-axis (horizontal line). A value of (0,0) means that
                the point will fall on none of the quadrants.
            </p>
            
        </div>
        <div class="col-md-4">
            <h2>Divisible Sizzable</h2>
            <p>
                An integer n is considered divisible if integer p can represent another integer k where the
                equation is p*k = n. An example is that 12 is divisible by 4 because 3*4 = 12.
            </p>
            
        </div>
        <div class="col-md-4">
            <h2>String Bling</h2>
            <p>
            A palindrome is a word that is the same when spelt backwards. Here is a list of common
            palindromes.For example “Reviver”, “Mom” ,“Dad” ,“Refer” ,“Defied” ,“Race Car” etc.            </p>
            
        </div>
    </div>

</asp:Content>
