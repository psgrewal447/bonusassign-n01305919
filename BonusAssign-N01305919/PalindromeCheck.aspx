﻿<%@ Page Title="String Bling" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PalindromeCheck.aspx.cs" Inherits="BonusAssign_N01305919.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="text-align:center">Enter to check wheather string is Palidrome or not</h2>
    <br />
   <div style="text-align:center">
    <asp:Label runat="server">Enter any String </asp:Label>
     
       <asp:TextBox runat="server" ID="validateString"></asp:TextBox>
       </br>
       <asp:RequiredFieldValidator ID="userInputStringRequired" runat="server" ControlToValidate="validateString" ErrorMessage="Value required."  ></asp:RequiredFieldValidator>
       <asp:RegularExpressionValidator ID="userInputStringRegularExpression" runat="server" ControlToValidate="validateString" ValidationExpression="[a-zA-Z ]+$" ErrorMessage="Please enter a valid string "  ></asp:RegularExpressionValidator>
       
        
       <br />
        <asp:Button ID="submitInput" Text="Check for Palindrome" runat="server" OnClick="CheckforPalindrome"/>
    </div>
    <div style="text-align:center" id="palidrome_check" runat="server"></div>
</asp:Content>
