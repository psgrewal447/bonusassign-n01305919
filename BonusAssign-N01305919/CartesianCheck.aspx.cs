﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssign_N01305919
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
 protected void Quardant(object sender, EventArgs e)
     {
       if (!Page.IsValid)
       {
          return;
       }

     double valueforxaxis = Convert.ToDouble(valueforXaxis.Text);
     double valueforyaxis = Convert.ToDouble(valueforYaxis.Text);
    if (valueforxaxis > 0 && valueforyaxis > 0)
            //just took the conditions from https://www.maplesoft.com/support/help/maple/view.aspx?path=MathApps%2FQuadrants
     {
          Quadrant_check.InnerHtml = "<strong>Result:</strong> (" + valueforXaxis.Text + "," + valueforYaxis.Text + ") is in Quadrant 1";
     }
     else if (valueforxaxis < 0 && valueforyaxis > 0)
     {
       Quadrant_check.InnerHtml = "<strong>Result:</strong> (" + valueforXaxis.Text + "," + valueforYaxis.Text + ") is in Quadrant 2";
      }
    else if (valueforxaxis < 0 && valueforyaxis < 0)
     {
     Quadrant_check.InnerHtml = "<strong>Result:</strong> (" + valueforXaxis.Text + "," + valueforYaxis.Text + ") is in Quadrant 3";
     }
     else if (valueforxaxis > 0 && valueforyaxis < 0)
     {
     Quadrant_check.InnerHtml = "<strong>Result:</strong> (" + valueforXaxis.Text + "," + valueforYaxis.Text + ") is in Quadrant 4";
      }
     }
     protected void valueforXAxisCustom_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(valueforXaxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
      protected void valueforYAxisCustom_ServerValidate(object source, ServerValidateEventArgs args)
        {
          if (Convert.ToDouble(valueforYaxis.Text) == 0)
          {
             args.IsValid = false;
           }
        }

    }
}